cmake_minimum_required(VERSION 2.8)

project( QtWebApp )

find_package(Qt4 REQUIRED)

set( QT_USE_QTNETWORK TRUE )

IF(MSVC)
ELSE()
  set( CMAKE_CXX_FLAGS_DEBUG "-O0 -g -Wall -std=c++11" )
  set( CMAKE_BUILD_TYPE Debug )

  set( QT_DONT_USE_QTGUI TRUE )
ENDIF(MSVC)

set( qtwebapp_SOURCES

  ./QtWebApp/httpserver/httpglobal.cpp
  ./QtWebApp/httpserver/httplistener.cpp
  ./QtWebApp/httpserver/httpconnectionhandler.cpp
  ./QtWebApp/httpserver/httpconnectionhandlerpool.cpp
  ./QtWebApp/httpserver/httprequest.cpp
  ./QtWebApp/httpserver/httpresponse.cpp
  ./QtWebApp/httpserver/httpcookie.cpp
  ./QtWebApp/httpserver/httprequesthandler.cpp
  ./QtWebApp/httpserver/httpsession.cpp
  ./QtWebApp/httpserver/httpsessionstore.cpp
  ./QtWebApp/httpserver/staticfilecontroller.cpp

  ./QtWebApp/logging/logmessage.cpp
  ./QtWebApp/logging/logger.cpp
  ./QtWebApp/logging/filelogger.cpp
  ./QtWebApp/logging/dualfilelogger.cpp

  ./QtWebApp/qtservice/qtservice.cpp

  ./QtWebApp/templateengine/template.cpp 
  ./QtWebApp/templateengine/templateloader.cpp 
  ./QtWebApp/templateengine/templatecache.cpp
  )

set( qtwebapp_HEADERS
  ./QtWebApp/httpserver/httpglobal.h
  ./QtWebApp/httpserver/httplistener.h
  ./QtWebApp/httpserver/httpconnectionhandler.h
  ./QtWebApp/httpserver/httpconnectionhandlerpool.h
  ./QtWebApp/httpserver/httprequest.h
  ./QtWebApp/httpserver/httpresponse.h
  ./QtWebApp/httpserver/httpcookie.h
  ./QtWebApp/httpserver/httprequesthandler.h
  ./QtWebApp/httpserver/httpsession.h
  ./QtWebApp/httpserver/httpsessionstore.h
  ./QtWebApp/httpserver/staticfilecontroller.h

  ./QtWebApp/logging/logglobal.h
  ./QtWebApp/logging/logmessage.h
  ./QtWebApp/logging/logger.h
  ./QtWebApp/logging/filelogger.h
  ./QtWebApp/logging/dualfilelogger.h

  ./QtWebApp/qtservice/qtservice.h
  ./QtWebApp/qtservice/qtservice_p.h

  ./QtWebApp/templateengine/templateglobal.h
  ./QtWebApp/templateengine/template.h 
  ./QtWebApp/templateengine/templateloader.h 
  ./QtWebApp/templateengine/templatecache.h
  )
  
SET( qtwebapp_LIBS )
SET( qtwebapp_DEFINES )
IF (MSVC)
  # WINDOWS specific files
  SET( qtwebapp_SOURCES
    ${qtwebapp_SOURCES}
    ./QtWebApp/qtservice/qtservice_win.cpp
    )

  SET( qtwebapp_LIBS
    ${qtwebapp_LIBS}
    user32
    )

  SET( qtwebapp_DEFINES
    ${qtwebapp_DEFINES}
    -DQTWEBAPPLIB_EXPORT
    )
ELSE ()
  # UNIX specific files
  SET( qtwebapp_SOURCES
    ${qtwebapp_SOURCES}
    ./QtWebApp/qtservice/qtservice_unix.cpp
    ./QtWebApp/qtservice/qtunixsocket.cpp
    ./QtWebApp/qtservice/qtunixserversocket.cpp
    )

  SET( qtwebapp_HEADERS
    ${qtwebapp_HEADERS}
    ./QtWebApp/qtservice/qtunixsocket.h
    ./QtWebApp/qtservice/qtunixserversocket.h
    )
ENDIF ()

qt4_wrap_cpp( qtwebapp_HEADERS_MOC ${qtwebapp_HEADERS} )

include( ${QT_USE_FILE} )
add_definitions( ${QT_DEFINITIONS} )

set( CMAKE_LIBRARY_PATH
  ${CMAKE_LIBRARY_PATH}
  )

add_library( QtWebApp
  ${qtwebapp_SOURCES} 
  ${qtwebapp_HEADERS_MOC}
  )

target_link_libraries( QtWebApp
  ${QT_LIBRARIES}
  ${qtwebapp_LIBS}
  )

include_directories(
  ${CMAKE_CURRENT_BINARY_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/QtWebApp/httpserver
  ${CMAKE_CURRENT_SOURCE_DIR}/QtWebApp/logging
  ${CMAKE_CURRENT_SOURCE_DIR}/QtWebApp/qtservice
  ${CMAKE_CURRENT_SOURCE_DIR}/QtWebApp/templateengine
  )

